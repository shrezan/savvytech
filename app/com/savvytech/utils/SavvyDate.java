package com.savvytech.utils;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/
public class SavvyDate {
    private DateTime dateTime;

    public static long timestamp() {
        return new SavvyDate().getDateTime().getMillis();
    }

    public static SavvyDate startOfMonth() {
        SavvyDate currentDate = new SavvyDate();
        int year = currentDate.getDate().getYear();
        int month = currentDate.getDate().getMonthOfYear();
        return new SavvyDate(year, month, 1, 0, 0, 0, 0);
    }

    public static SavvyDate endOfMonth() {
        SavvyDate currentDate = startOfMonth();
        currentDate.addMonths(1);
        currentDate.minusDays(1);
        int year = currentDate.getDate().getYear();
        int month = currentDate.getDate().getMonthOfYear();
        int day = currentDate.getDate().getDayOfMonth();
        return new SavvyDate(year, month, day, 23, 59, 59, 999);
    }

    public static boolean isSameDate(long first, long second) {
        SavvyDate txnDate = new SavvyDate(first);
        SavvyDate currentDate = new SavvyDate(second);
        return currentDate.getDate().toLocalDate().isEqual(txnDate.getDate().toLocalDate());
    }

    public static SavvyDate startOfDay() {
        SavvyDate currentDate = new SavvyDate();
        int year = currentDate.getDate().getYear();
        int month = currentDate.getDate().getMonthOfYear();
        int day = currentDate.getDate().getDayOfMonth();
        return new SavvyDate(year, month, day, 0, 0, 0, 0);
    }

    public static SavvyDate parse(String date, String format) {
        DateTimeFormatter dateTimeFormat = DateTimeFormat.forPattern(format);
        return new SavvyDate(dateTimeFormat.parseDateTime(date));
    }

    public static SavvyDate endOfDay() {
        SavvyDate currentDate = new SavvyDate();
        int year = currentDate.getDate().getYear();
        int month = currentDate.getDate().getMonthOfYear();
        int day = currentDate.getDate().getDayOfMonth();
        return new SavvyDate(year, month, day, 23, 59, 59, 999);
    }

    public SavvyDate(String timeZone) {
        dateTime = new DateTime(DateTimeZone.forID(timeZone));
    }

    public SavvyDate() {
        dateTime = new DateTime(DateTimeZone.UTC);
    }

    private SavvyDate(DateTime dateTime) {
        this.dateTime = dateTime;
    }

    public SavvyDate(int year, int month, int date, int hour, int min, int sec, int milli) {
        dateTime = new DateTime(year, month, date, hour, min, sec, milli, DateTimeZone.UTC);
    }

    public SavvyDate(int year, int month, int date, int hour, int min, int sec, int milli, String timeZone) {
        dateTime = new DateTime(year, month, date, hour, min, sec, milli, DateTimeZone.
                forID(timeZone));
    }

    public SavvyDate(long timestamp) {
        dateTime = new DateTime(timestamp);
    }

    public String format(String format) {
        return dateTime.toString(format);
    }

    /**
     * Get Date instance for given timezone
     *
     * @return
     */
    public DateTime getDate(String timeZone) {
        DateTime dateTime = this.dateTime.withZone(DateTimeZone.forID(timeZone));
        return new DateTime(dateTime.getYear(), dateTime.getMonthOfYear(),
                dateTime.getDayOfMonth(), 0, 0, 0, 0, DateTimeZone.forID(timeZone));
    }

    public DateTime getDate() {
        return new DateTime(this.dateTime.getYear(), this.dateTime.getMonthOfYear(),
                this.dateTime.getDayOfMonth(), 0, 0, 0, 0, DateTimeZone.UTC);

    }

    /**
     * Get DateTime instance for given timezone.
     *
     * @param timeZone
     * @return
     */
    public DateTime getDateTime(String timeZone) {
        return this.dateTime.withZone(DateTimeZone.forID(timeZone));
    }

    public DateTime getDateTime() {
        return this.dateTime;

    }

    public DateTime getStartOfDay() {
        return this.dateTime.withTimeAtStartOfDay();
    }

    public DateTime getTimeOfDay(int hour, int minutes, int seconds, int millis) {
        return this.dateTime.withTimeAtStartOfDay()
                .plusHours(hour)
                .plusMinutes(minutes)
                .plusSeconds(seconds)
                .plusMillis(millis);
    }

    public DateTime getEndOfDay() {
        return this.dateTime.withTimeAtStartOfDay()
                .plusHours(23)
                .plusMinutes(59).plusSeconds(59)
                .plusMillis(999);
    }

    public String getZoneId() {
        return this.dateTime.getZone().getID();
    }

    public long getTimestamp() {
        return this.dateTime.getMillis();
    }

    public String toString() {
        return dateTime.toString();
    }

    public void addYears(int year) {
        this.dateTime = this.dateTime.plusYears(year);
    }

    public void addDays(int day) {
        this.dateTime = this.dateTime.plusDays(day);
    }

    public void addHours(int hour) {
        this.dateTime = this.dateTime.plusHours(hour);
    }

    public void addMinutes(int minute) {
        this.dateTime = this.dateTime.plusMinutes(minute);
    }

    public void addSeconds(int seconds) {
        this.dateTime = this.dateTime.plusSeconds(seconds);
    }

    public void addMonths(int months) {
        this.dateTime = this.dateTime.plusMonths(months);
    }

    public void addMilliseconds(int millis) {
        this.dateTime = this.dateTime.plusMillis(millis);
    }

    public void minusMonth(int months) {
        this.dateTime = this.dateTime.minusMonths(months);
    }

    public void minusDays(int day) {
        this.dateTime = this.dateTime.minusDays(day);
    }

    public void minusHours(int hours) {
        this.dateTime = this.dateTime.minusHours(hours);
    }

    public void minusMinutes(int minute) {
        this.dateTime = this.dateTime.minusMinutes(minute);
    }

    public void minusSeconds(int seconds) {
        this.dateTime = this.dateTime.minusSeconds(seconds);
    }

}

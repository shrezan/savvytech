package com.savvytech.utils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/
public final class AsyncProcessor {
    private static final ExecutorService executorService = Executors.newFixedThreadPool(2);

    private AsyncProcessor() {
    }

    public static ExecutorService getService() {
        return executorService;
    }

    public static void shutdown() {
        executorService.shutdown();
    }
}

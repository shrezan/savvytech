package com.savvytech.utils.http;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/
public class HttpException extends Exception {
    public HttpException(Throwable throwable) {
        super(throwable);
    }
}

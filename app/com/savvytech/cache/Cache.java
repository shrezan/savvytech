package com.savvytech.cache;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/
public interface Cache<T> {
    boolean set(String key, T value, int ttl);

    boolean set(String key, T value);

    T get(String key);

    boolean delete(String... key);
}

package com.savvytech.cache;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/
public interface CacheManager<T extends Cache> {
    void start();

    default void start(int database) {
    }

    default void start(String host, int port, String password, int database) {
    }

    default void start(String host, int port, int database) {
    }

    default void start(String host, int port) {
    }

    T getCache();

    void stop();
}

package com.savvytech.services;

import com.savvytech.entities.AccountProto;
import com.savvytech.entities.FileDetail;
import com.savvytech.entities.ReqResProto;

import javax.inject.Inject;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-06-22
 **/
public abstract class FileUploadService extends Service {
    @Inject
    public FileUploadService(AccountService accountService) {
        super(accountService);
    }

    public abstract ReqResProto.Response uploadFile(AccountProto.Authorization authorization,
                                                    FileDetail fileDetail);
}

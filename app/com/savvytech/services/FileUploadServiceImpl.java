package com.savvytech.services;

import com.savvytech.entities.AccountProto;
import com.savvytech.entities.FileDetail;
import com.savvytech.entities.ReqResProto;
import com.savvytech.repo.FileRepo;
import com.savvytech.utils.SavvyUtils;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-06-22
 **/
public final class FileUploadServiceImpl extends FileUploadService {
    private static final Logger logger = LoggerFactory.getLogger(FileUploadServiceImpl.class);
    private static final Config config = ConfigFactory.load();
    private static final String MINIO_BUCKET = config.getString("savvy.upload.bucket");

    private final AccountService accountService;
    private final FileRepo fileRepo;

    @Inject
    public FileUploadServiceImpl(final FileRepo fileRepo, AccountService accountService) {
        super(accountService);
        this.fileRepo = fileRepo;
        this.accountService = accountService;
    }

    @Override
    public ReqResProto.Response uploadFile(AccountProto.Authorization authorization,
                                           FileDetail fileDetail) {
        try {
            accountService.authorize(authorization);
            String extension = fileDetail.getMime().substring(fileDetail.getMime().lastIndexOf("/") + 1);
            if (!StringUtils.equals("pdf", extension) || !StringUtils.equals("doc", extension)) {
                String fileName = SavvyUtils.uuid() + "." + extension;
                if (this.fileRepo.uploadFile(MINIO_BUCKET, fileName, fileDetail.getMime(), fileDetail.getStream())) {
                    return ReqResProto.Response.newBuilder()
                            .setError(false)
                            .setPath(MINIO_BUCKET + ":" + fileName)
                            .setUrl(this.fileRepo.generatePublicUrl(MINIO_BUCKET, fileName))
                            .build();
                }
                return errorResponse("Could not upload file");
            } else {
                return errorResponse("Could not upload " + extension + " file. " +
                        "Please upload the file in pdf or doc format.");
            }
        } catch (Exception e) {
            logger.error("Error while getting input stream", e);
            return errorResponse("Error while uploading file.");
        }
    }

    private ReqResProto.Response errorResponse(String msg) {
        return ReqResProto.Response.newBuilder()
                .setError(true)
                .setMsg(msg)
                .build();
    }
}

package com.savvytech.services;

import com.savvytech.entities.AccountProto;
import com.savvytech.entities.ReqResProto;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/
public abstract class AccountService extends Service {
    public AccountService(AccountService accountService) {
        super(accountService);
    }

    public abstract ReqResProto.Response login(AccountProto.LoginRequest loginRequest);

    public abstract ReqResProto.Response authorize(final AccountProto.Authorization authorization);

    public abstract ReqResProto.Response logout(AccountProto.Authorization authorization);

}

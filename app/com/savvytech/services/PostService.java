package com.savvytech.services;

import com.savvytech.entities.AccountProto;
import com.savvytech.entities.ReqResProto;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-05
 **/
public abstract class PostService extends com.savvytech.services.Service {
    public PostService(AccountService accountService) {
        super(accountService);
    }

    public abstract ReqResProto.Response addPost(AccountProto.Post post);

    public abstract ReqResProto.Response getEvents(AccountProto.Authorization authorization);

    public abstract ReqResProto.Response getJobs(AccountProto.Authorization authorization, long to, int limit);

    public abstract ReqResProto.Response getPostById(AccountProto.Authorization authorization, String postId);

    public abstract ReqResProto.Response postApply(AccountProto.Authorization authorization, AccountProto.Apply apply);

    public abstract ReqResProto.Response getPostApplies(AccountProto.Authorization authorization,
                                                        String postId, long to, int limit);

    public abstract ReqResProto.Response getCompanyPosts(AccountProto.Authorization authorization,
                                                         String companyId, long to, int limit);

    public abstract ReqResProto.Response getPostsByPostType(AccountProto.Authorization authorization,
                                                            String postType, long to, int limit);

    public abstract ReqResProto.Response deletePost(AccountProto.Authorization authorization, String postId);
}

package com.savvytech.services;

import com.savvytech.entities.AccountProto;
import com.savvytech.entities.ReqResProto;
import com.savvytech.repo.UserRepo;
import com.savvytech.utils.SavvyDate;
import com.savvytech.utils.SavvyUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/
public final class UserServiceImpl extends UserService {
    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
    private final com.savvytech.repo.UserRepo userRepo;

    @Inject
    public UserServiceImpl(AccountService accountService, UserRepo userRepo) {
        super(accountService);
        this.userRepo = userRepo;
    }

    @Override
    public ReqResProto.Response addUser(AccountProto.User user, String password) {
        try {
            if (StringUtils.isBlank(user.getEmail())) {
                logger.debug("Email not specified");
                return invalid("Email not specified");
            }
            if (StringUtils.isBlank(password)) {
                logger.debug("Password not specified");
                return invalid("Password not specified");
            }
            if (user.getUserType() == AccountProto.UserType.UNKNOWN_USER_TYPE) {
                logger.debug("User Type not specified");
                return invalid("User Type not specified");
            }
            if (userRepo.getUserByEmail(user.getEmail()) != null) {
                logger.debug("User already exists.");
                return duplicate("User already exists.");
            }
            user = user.toBuilder()
                    .setUserId(SavvyUtils.uuid())
                    .setCreatedAt(SavvyDate.timestamp())
                    .build();
            if (userRepo.addUser(user, password)) {
                return ReqResProto.Response.newBuilder()
                        .setUser(user)
                        .build();
            }
            logger.debug("Failed to add user.");
            return failed("Failed to add user.");
        } catch (Exception ex) {
            logger.error("Error while adding user.", ex);
            return exception(ex, logger);
        }
    }

    @Override
    public ReqResProto.Response getStudents(AccountProto.Authorization authorization) {
        try {
            this.authorize(authorization, "user.get");
            List<AccountProto.User> users = userRepo.getStudents();
            return ReqResProto.Response.newBuilder()
                    .addAllUsers(users)
                    .build();
        } catch (Exception ex) {
            logger.error("Error while getting students details.", ex);
            return exception(ex, logger);
        }
    }

    @Override
    public ReqResProto.Response getCompanies(AccountProto.Authorization authorization) {
        try {
            this.authorize(authorization, "user.get");
            List<AccountProto.User> users = userRepo.getCompanies();
            return ReqResProto.Response.newBuilder()
                    .addAllUsers(users)
                    .build();
        } catch (Exception ex) {
            logger.error("Error while getting companies details.", ex);
            return exception(ex, logger);
        }
    }

    @Override
    public ReqResProto.Response getUserById(AccountProto.Authorization authorization,
                                            String userId) {
        try {
            this.authorize(authorization, "user.get");
            if (null == userId) {
                logger.debug("User Id not provided");
                return invalid("User Id not provided");
            }
            AccountProto.User user = userRepo.getUserById(userId);
            return ReqResProto.Response.newBuilder()
                    .setUser(user)
                    .build();
        } catch (Exception ex) {
            logger.error("Error while getting user details.", ex);
            return exception(ex, logger);
        }
    }

    @Override
    public ReqResProto.Response getUserByEmail(AccountProto.Authorization authorization, String email) {
        try {
            this.authorize(authorization, "user.get");
            if (null == email) {
                logger.debug("Email not provided");
                return invalid("Email not provided");
            }
            AccountProto.User user = userRepo.getUserByEmail(email);
            return ReqResProto.Response.newBuilder()
                    .setUser(user)
                    .build();
        } catch (Exception ex) {
            logger.error("Error while getting user details.", ex);
            return exception(ex, logger);
        }
    }
}

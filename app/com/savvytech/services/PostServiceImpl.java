package com.savvytech.services;

import com.google.inject.Inject;
import com.savvytech.entities.AccountProto;
import com.savvytech.entities.ReqResProto;
import com.savvytech.repo.PostRepo;
import com.savvytech.utils.SavvyDate;
import com.savvytech.utils.SavvyUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-05
 **/
public final class PostServiceImpl extends PostService {
    private static final Logger logger = LoggerFactory.getLogger(PostServiceImpl.class);
    private final com.savvytech.repo.PostRepo postRepo;

    @Inject
    public PostServiceImpl(AccountService accountService, PostRepo postRepo) {
        super(accountService);
        this.postRepo = postRepo;
    }

    @Override
    public ReqResProto.Response addPost(AccountProto.Post post) {
        try {
            if (StringUtils.isBlank(post.getTitle())) {
                return invalid("Title not specified");
            }
            post = post.toBuilder()
                    .setPostId(SavvyUtils.uuid())
                    .setCreatedAt(SavvyDate.timestamp())
                    .build();
            if (postRepo.addPost(post)) {
                return ReqResProto.Response.newBuilder()
                        .setPost(post)
                        .build();
            }
            return failed("Failed to add post.");
        } catch (Exception ex) {
            logger.error("Error while adding post.", ex);
            return exception(ex, logger);
        }
    }

    @Override
    public ReqResProto.Response getEvents(AccountProto.Authorization authorization) {
        try {
            this.authorize(authorization, "event.get");
            List<AccountProto.Post> posts = postRepo.getEvents();
            return ReqResProto.Response.newBuilder()
                    .addAllPosts(posts)
                    .build();
        } catch (Exception ex) {
            logger.error("Error while getting events details.", ex);
            return exception(ex, logger);
        }
    }

    @Override
    public ReqResProto.Response getJobs(AccountProto.Authorization authorization, long to, int limit) {
        try {
            this.authorize(authorization, "job.get");
            List<AccountProto.Post> posts = postRepo.getJobs(to, limit);
            return ReqResProto.Response.newBuilder()
                    .addAllPosts(posts)
                    .build();
        } catch (Exception ex) {
            logger.error("Error while getting jobs details.", ex);
            return exception(ex, logger);
        }
    }

    @Override
    public ReqResProto.Response getPostById(AccountProto.Authorization authorization, String postId) {
        try {
            this.authorize(authorization, "post.get");
            AccountProto.PostUser postUser = postRepo.getPostById(postId);
            return ReqResProto.Response.newBuilder()
                    .setPostUser(postUser)
                    .build();
        } catch (Exception ex) {
            logger.error("Error while getting events details.", ex);
            return exception(ex, logger);
        }
    }

    @Override
    public ReqResProto.Response postApply(AccountProto.Authorization authorization, AccountProto.Apply apply) {
        try {
            this.authorize(authorization, "post.apply");
            if(postRepo.hasUserApplied(apply.getStudentId(), apply.getPostId())) {
                return invalid("Already applied to this job.");
            }
            apply = apply.toBuilder()
                    .setApplyId(SavvyUtils.uuid())
                    .setCreatedAt(SavvyDate.timestamp())
                    .build();
            if (postRepo.postApply(apply)) {
                return ReqResProto.Response.newBuilder()
                        .setApply(apply)
                        .build();
            }
            return failed("Failed to add post.");
        } catch (Exception ex) {
            logger.error("Error while adding post.", ex);
            return exception(ex, logger);
        }
    }

    @Override
    public ReqResProto.Response getPostApplies(AccountProto.Authorization authorization,
                                               String postId, long to, int limit) {
        try {
            this.authorize(authorization, "apply.get");
            List<AccountProto.Apply> applies = postRepo.getPostApplies(postId, to, limit);
            return ReqResProto.Response
                    .newBuilder()
                    .addAllApplies(applies)
                    .build();
        } catch (Exception ex) {
            logger.error("Error while getting apply details.", ex);
            return exception(ex, logger);
        }
    }

    @Override
    public ReqResProto.Response getCompanyPosts(AccountProto.Authorization authorization,
                                                String companyId, long to, int limit) {
        try {
            this.authorize(authorization, "posts.get");
            List<AccountProto.Post> posts = postRepo.getCompanyPosts(companyId, to, limit);
            return ReqResProto.Response
                    .newBuilder()
                    .addAllPosts(posts)
                    .build();
        } catch (Exception ex) {
            logger.error("Error while getting posts details.", ex);
            return exception(ex, logger);
        }
    }

    @Override
    public ReqResProto.Response deletePost(AccountProto.Authorization authorization, String postId) {
        try {
            this.authorize(authorization, "post.delete");
            if(postRepo.deletePost(postId)) {
                return ReqResProto.Response
                        .newBuilder()
                        .build();
            }
            return failed("Failed to delete post.");
        } catch (Exception ex) {
            logger.error("Error while getting posts details.", ex);
            return exception(ex, logger);
        }
    }

    @Override
    public ReqResProto.Response getPostsByPostType(AccountProto.Authorization authorization,
                                                   String postType,
                                                   long to,
                                                   int limit) {
        try {
            this.authorize(authorization, "posts.get");
            AccountProto.PostType postTypeValue = AccountProto.PostType.valueOf(postType);
            if(postTypeValue == AccountProto.PostType.UNKNOWN_POST_TYPE) {
                logger.debug("Invalid postType: {}", postType);
                return invalid("Invalid postType.");
            }
            if(to < 0) {
                logger.debug("Invalid time: {}", to);
                return invalid("Invalid time.");
            }
            if(limit < 0) {
                logger.debug("Invalid limit: {}", limit);
                return invalid("Invalid limit.");
            }
            List<AccountProto.Post> posts = postRepo.getPostsByPostType(postTypeValue, to, limit);
            return ReqResProto.Response
                    .newBuilder()
                    .addAllPosts(posts)
                    .build();
        } catch (Exception ex) {
            logger.error("Error while getting posts details.", ex);
            return exception(ex, logger);
        }
    }
}

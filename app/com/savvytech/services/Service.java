package com.savvytech.services;

import com.savvytech.entities.AccountProto;
import com.savvytech.entities.ReqResProto;
import com.savvytech.ex.AuthException;
import com.savvytech.ex.PermissionException;
import org.slf4j.Logger;

import javax.inject.Inject;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/
abstract class Service {
    private final AccountService accountService;

    @Inject
    public Service(final AccountService accountService) {
        this.accountService = accountService;
    }

    public ReqResProto.Response invalid(String msg) {
        return ReqResProto.Response
                .newBuilder()
                .setError(true)
                .setMsg(msg)
                .setErrorCode(ReqResProto.ErrorCode.INVALID)
                .build();
    }

    public ReqResProto.Response failed(String msg) {
        return ReqResProto.Response
                .newBuilder()
                .setError(true)
                .setMsg(msg)
                .setErrorCode(ReqResProto.ErrorCode.FAILED)
                .build();
    }

    public ReqResProto.Response duplicate(String msg) {
        return ReqResProto.Response
                .newBuilder()
                .setError(true)
                .setMsg(msg)
                .setErrorCode(ReqResProto.ErrorCode.DUPLICATE)
                .build();
    }

    public ReqResProto.Response exception(String msg) {
        return ReqResProto.Response
                .newBuilder()
                .setError(true)
                .setMsg(msg)
                .setErrorCode(ReqResProto.ErrorCode.EXCEPTION)
                .build();
    }

    public ReqResProto.Response notfound(String msg) {
        return ReqResProto.Response
                .newBuilder()
                .setError(true)
                .setMsg(msg)
                .setErrorCode(ReqResProto.ErrorCode.NOT_FOUND)
                .build();
    }

    public AccountProto.AuthResponse authorize(AccountProto.Authorization authorization, String permission)
            throws AuthException {
        ReqResProto.Response response = this.accountService
                .authorize(authorization.toBuilder().setPermission(permission).build());
        if (null == response) {
            throw new AuthException();
        }
        if (response.getError()) {
            throw new AuthException();
        }
        if (!response.getAuthResponse().getGranted()) {
            throw new PermissionException();
        }
        return response.getAuthResponse();
    }

    public ReqResProto.Response exception(Throwable throwable, final Logger logger) {
        ReqResProto.Response.Builder builder = ReqResProto.Response.newBuilder()
                .setError(true);
        if (throwable instanceof PermissionException) {
            builder.setMsg("User don't have permission.");
            builder.setErrorCode(ReqResProto.ErrorCode.UNAUTHORIZED);
        } else if (throwable instanceof AuthException) {
            builder.setMsg("Authorization failed.");
            builder.setErrorCode(ReqResProto.ErrorCode.UNAUTHORIZED);
        } else {
            builder.setMsg(throwable.getMessage() != null ? throwable.getMessage() : "Server exception");
            builder.setErrorCode(ReqResProto.ErrorCode.EXCEPTION);
        }
        if (null != logger && !(throwable instanceof AuthException)) {
            logger.error("Exception in server", throwable);
        }
        return builder.build();
    }
}

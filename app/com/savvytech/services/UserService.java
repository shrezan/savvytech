package com.savvytech.services;

import com.savvytech.entities.AccountProto;
import com.savvytech.entities.ReqResProto;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/
public abstract class UserService extends Service {
    public UserService(AccountService accountService) {
        super(accountService);
    }

    public abstract ReqResProto.Response addUser(AccountProto.User user, String password);

    public abstract ReqResProto.Response getStudents(AccountProto.Authorization authorization);

    public abstract ReqResProto.Response getCompanies(AccountProto.Authorization authorization);

    public abstract ReqResProto.Response getUserById(AccountProto.Authorization authorization, String userId);

    public abstract ReqResProto.Response getUserByEmail(AccountProto.Authorization authorization, String userId);
}



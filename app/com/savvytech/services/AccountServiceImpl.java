package com.savvytech.services;

import com.savvytech.entities.AccountProto;
import com.savvytech.entities.ReqResProto;
import com.savvytech.repo.AuthRepo;
import com.savvytech.repo.UserRepo;
import com.savvytech.repo.model.LoginResult;
import com.savvytech.utils.SavvyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.nio.charset.StandardCharsets;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/
public final class AccountServiceImpl extends AccountService {
    private static final Logger logger = LoggerFactory.getLogger(AccountServiceImpl.class);
    private final UserRepo userRepo;
    private final AuthRepo authRepo;
    private final ReqResProto.Response invalidResponse = invalid("Invalid username or password.");

    @Inject
    public AccountServiceImpl(final UserRepo userRepo,
                              final AuthRepo authRepo) {
        super(null);
        this.userRepo = userRepo;
        this.authRepo = authRepo;
    }

    @Override
    public ReqResProto.Response login(AccountProto.LoginRequest loginRequest) {
        String email = loginRequest.getEmail();
        String password = loginRequest.getPassword();
        if (null == email || email.trim().length() <= 0) {
            return invalidResponse;
        }
        if (null == password) {
            return invalidResponse;
        }
        try {
            LoginResult loginResult = userRepo.login(email);
            if (null == loginResult) {
                return invalidResponse;
            } else {
                if (SavvyUtils.checkPassword(password, loginResult.password)) {
                    String authToken = createSession(loginResult.user, loginRequest);
                    if (null == authToken) {
                        logger.error("Could not create session after login.");
                        return failed("Could not create session after login.");
                    } else {
                        AccountProto.LoginResponse loginResponse = AccountProto.LoginResponse.newBuilder()
                                .setToken(authToken)
                                .setUser(loginResult.user)
                                .build();
                        return ReqResProto.Response.newBuilder()
                                .setError(false)
                                .setLoginResponse(loginResponse)
                                .build();
                    }
                } else {
                    return invalid("Invalid username or password.");
                }
            }
        } catch (Exception ex) {
            return exception(ex, logger);
        }
    }

    public ReqResProto.Response authorize(final AccountProto.Authorization authorization) {
        try {
            final AccountProto.Session session = authorize(authorization.getToken());
            if (null == session) {
                return ReqResProto.Response.newBuilder()
                        .setError(true)
                        .setMsg("Authorization failed.")
                        .setErrorCode(ReqResProto.ErrorCode.UNAUTHORIZED)
                        .build();
            }
            return buildAuthResponse(session,
                    this.permission(authorization.getPermission()));
        } catch (Exception ex) {
            return exception(ex, logger);
        }
    }

    public ReqResProto.Response logout(AccountProto.Authorization authorization) {
        try {
            final AccountProto.Session session = authorize(authorization.getToken());
            if (null != session) {
                authRepo.remove(session);
            }
            logger.debug("Logged out successfully.");
            return ReqResProto.Response.newBuilder()
                    .setError(false)
                    .setSuccess(true)
                    .build();
        } catch (Exception e) {
            return exception(e, logger);
        }
    }

    private ReqResProto.Response buildAuthResponse(AccountProto.Session session, boolean granted) {
        AccountProto.AuthResponse authResponse = AccountProto.AuthResponse.newBuilder()
                .setGranted(granted)
                .setSession(session)
                .build();
        return ReqResProto.Response.newBuilder()
                .setAuthResponse(authResponse)
                .build();
    }

    private boolean permission(String permission) {
        return null != permission;
    }

    private AccountProto.Session authorize(final String authorization) {
        if (null == authorization) return null;
        String[] authParts = authorization.split("\\.");
        if (authParts.length == 2) {
            String payload = SavvyUtils.decodeBase64(authParts[0]);
            String signature = authParts[1];
            String[] payloadParts = payload.split("\\.");
            if (payloadParts.length == 2) {
                String userId = payloadParts[0];
                String sessionId = payloadParts[1];
                AccountProto.Jwt session = authRepo.check(userId, sessionId);
                if (null != session) {
                    String checkSignature = SavvyUtils.hmacSha512(session.getSignatureSecret(),
                            SavvyUtils.getBytes(payload));
                    if (check(signature, checkSignature)) {
                        return session.getSession();
                    }
                }
            }
        }
        return null;
    }

    private boolean check(String one, String two) {
        try {
            byte[] oneBytes = one.getBytes(StandardCharsets.UTF_8);
            byte[] twoBytes = two.getBytes(StandardCharsets.UTF_8);
            if (oneBytes.length != twoBytes.length) {
                return false;
            }
            byte ret = 0;
            for (int i = 0; i < twoBytes.length; i++) {
                ret |= oneBytes[i] ^ twoBytes[i];
            }
            return ret == 0;
        } catch (Exception e) {
            logger.error("Error while getting bytes.", e);
        }
        return false;
    }

    private String createSession(final AccountProto.User user, final AccountProto.LoginRequest login) {
        final AccountProto.Session.Builder session = AccountProto.Session.newBuilder();
        session.setUser(user);
        session.setUserId(user.getUserId());
        session.setSessionId(SavvyUtils.uuid());
        session.setDeviceType(login.getDeviceType());
        String secret = SavvyUtils.uuid();
        AccountProto.Jwt jwt = AccountProto.Jwt.newBuilder()
                .setSession(session)
                .setSignatureSecret(secret)
                .build();
        if (authRepo.save(jwt)) {
            String payload = String.format("%s.%s", session.getUser().getUserId(), session.getSessionId());
            final String signature = SavvyUtils.hmacSha512(secret, SavvyUtils.getBytes(payload));
            return String.format("%s.%s", SavvyUtils.encodeBase64(payload), signature);
        } else {
            return null;
        }
    }
}

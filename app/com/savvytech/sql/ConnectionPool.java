package com.savvytech.sql;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/
final class ConnectionPool {
    private final DataSource dataSource;
    private static final Config config = ConfigFactory.load();
    private static final Logger logger = LoggerFactory.getLogger(ConnectionPool.class);

    static ConnectionPool create(final DbInfo dbInfo) throws JDBCException {
        if (dbInfo == null) throw new JDBCException("DbInfo was not initialized.");
        String db = dbInfo.getDatabaseName();
        final PoolProperties properties = new PoolProperties();
        properties.setUrl(dbInfo.getUrl());
        properties.setDriverClassName(dbInfo.getDriver());
        properties.setPassword(dbInfo.getPassword());
        properties.setUsername(dbInfo.getUsername());
        properties.setJmxEnabled(true);
        properties.setTestWhileIdle(false);
        properties.setTestOnBorrow(true);
        if (null == dbInfo.getValidationQuery()) {
            dbInfo.setValidationQuery("SELECT 1");
        }
        properties.setValidationQuery(dbInfo.getValidationQuery());
        properties.setValidationInterval(30000);
        properties.setTimeBetweenEvictionRunsMillis(5000);
        properties.setMaxActive(config.getInt(getConfigName("pool.max.active", db)));
        properties.setInitialSize(config.getInt(getConfigName("pool.initial.size", db)));
        properties.setMaxWait(config.getInt(getConfigName("pool.max.wait", db)));
        properties.setRemoveAbandonedTimeout(config
                .getInt(getConfigName("pool.removeAbandonedTimeout", db)));
        properties.setMinEvictableIdleTimeMillis(30000);
        properties.setMinIdle(config.getInt(getConfigName("pool.min.idle", db)));
        properties.setMaxIdle(config.getInt(getConfigName("pool.max.idle", db)));
        properties.setLogAbandoned(true);
        properties.setRemoveAbandoned(true);
        properties.setAbandonWhenPercentageFull(50);
        properties.setJdbcInterceptors("org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;" +
                "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer;" +
                "org.apache.tomcat.jdbc.pool.interceptor.ResetAbandonedTimer"
        );
        if (dbInfo.getDataSourceClass() == null) {
            dbInfo.setDataSourceClass("org.apache.tomcat.jdbc.pool.DataSource");
        }
        return new ConnectionPool(properties, dbInfo.getDataSourceClass());
    }

    private static String getConfigName(String config, String db) {
        return "db." + db + "." + config;
    }

    private ConnectionPool(PoolProperties properties, String dataSourceClass) {
        logger.debug("Using datasource class : {}", dataSourceClass);
        this.dataSource = new DataSource();
        this.dataSource.setPoolProperties(properties);
    }

    Connection getConnection() throws JDBCException {
        if (this.dataSource == null)
            throw new RuntimeException("Pool not initialized.");
        try {
            return this.dataSource.getConnection();
        } catch (SQLException e) {
            throw new JDBCException(e);
        }
    }

    void destroy() {
        if (this.dataSource != null)
            this.dataSource.close();
    }
}
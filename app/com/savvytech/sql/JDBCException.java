package com.savvytech.sql;

import com.savvytech.ex.SavvyException;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/
public class JDBCException extends SavvyException {
    public JDBCException(String message) {
        super(message);
    }

    public JDBCException(Throwable throwable) {
        super(throwable);
    }
}
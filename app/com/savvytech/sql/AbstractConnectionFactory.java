package com.savvytech.sql;

import java.sql.Connection;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/
public abstract class AbstractConnectionFactory implements ConnectionFactory {

    private ConnectionPool connectionPool;

    public void start() throws JDBCException {
        DbInfo dbInfo = this.config();
        connectionPool = ConnectionPool.create(dbInfo);
    }

    public Connection getConnection() throws JDBCException {
        return this.connectionPool.getConnection();
    }

    public void shutdown() {
        this.connectionPool.destroy();
    }
}

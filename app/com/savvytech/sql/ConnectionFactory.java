package com.savvytech.sql;

import java.sql.Connection;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/
public interface ConnectionFactory {

    void start() throws JDBCException;

    DbInfo config();

    Connection getConnection() throws JDBCException;

    void shutdown();
}

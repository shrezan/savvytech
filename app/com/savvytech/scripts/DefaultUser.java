package com.savvytech.scripts;

import com.savvytech.entities.AccountProto;
import com.savvytech.ex.SavvyException;
import com.savvytech.repo.SavvytechDataFactory;
import com.savvytech.repo.UserRepo;
import com.savvytech.repo.UserRepoImpl;
import com.savvytech.sql.SqlDataSource;
import com.savvytech.utils.SavvyDate;
import com.savvytech.utils.SavvyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/
public class DefaultUser {
    private static Logger logger = LoggerFactory.getLogger(DefaultUser.class);

    public static void main(String[] args) {
        try {
            SavvytechDataFactory savvytechDataFactory = SavvytechDataFactory.getInstance();
            SqlDataSource sqlDataSource = new SqlDataSource(savvytechDataFactory);
            UserRepo userRepo = new UserRepoImpl(sqlDataSource);

            AccountProto.User user = userRepo.getUserById("admin");
            if (user == null) {
                String userId = SavvyUtils.uuid();
                user = AccountProto.User.newBuilder()
                        .setUserId(userId)
                        .setName("Admin")
                        .setCreatedAt(SavvyDate.timestamp())
                        .setQualification(AccountProto.Qualification.BACHELORS)
                        .setUserType(AccountProto.UserType.STUDENT)
                        .setEmail("Admin@admin.com")
                        .setAddress("admin")
                        .setPhone("9800000000")
                        .build();
                boolean result = userRepo.addUser(user, SavvyUtils.passwordHash("admin"));
                logger.info("{}", result);
            } else {
                logger.info("User already exists.");
            }
        } catch (SavvyException e) {
            logger.error("Error while creating the default user", e);
        }
    }
}

package com.savvytech.scripts;

import com.savvytech.ex.SavvyException;
import com.savvytech.repo.SavvytechDataFactory;
import com.savvytech.repo.UserRepo;
import com.savvytech.repo.UserRepoImpl;
import com.savvytech.repo.model.LoginResult;
import com.savvytech.sql.SqlDataSource;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/
public class LoginUser {
    public static void main(String[] args) throws SavvyException {
        SavvytechDataFactory savvytechDataFactory = SavvytechDataFactory.getInstance();
        SqlDataSource sqlDataSource = new SqlDataSource(savvytechDataFactory);
        UserRepo userRepo = new UserRepoImpl(sqlDataSource);
        LoginResult result = userRepo.login("admin");
        System.out.println(result.user);
        System.out.println(result.password);
    }
}

package com.savvytech.repo;

import com.savvytech.entities.AccountProto;
import com.savvytech.ex.SavvyException;
import com.savvytech.repo.model.LoginResult;
import com.savvytech.sql.JDBCException;
import com.savvytech.sql.SqlDataSource;
import com.savvytech.utils.SavvyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/
public final class UserRepoImpl extends UserRepo {
    private static final Logger logger = LoggerFactory.getLogger(UserRepoImpl.class);

    private static final String USER_TABLE = "user";
    private static final String USER_COLS = "user_id, user_type, full_name, qualification, email, address, phone, " +
            "created_at, updated_at";

    private static final String QUERY = "Query : {}";
    private static final String SELECT = "SELECT ";

    @Inject
    public UserRepoImpl(SqlDataSource dataSource) {
        super(dataSource);
    }

    @Override
    public LoginResult login(String email) throws SavvyException {
        String query = SELECT + USER_COLS + ", password FROM " + USER_TABLE + " WHERE email = ?";
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            logger.debug(QUERY, query);
            statement = dataSource.prepareStatement(query, connection);
            statement.setString(1, email);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                AccountProto.User user = extractUser(resultSet);
                String password = resultSet.getString("password");
                return new LoginResult(password, user);
            } else {
                return null;
            }
        } catch (SQLException | JDBCException ex) {
            throw new SavvyException("Error while getting user", ex);
        } finally {
            close(resultSet);
            close(statement);
            close(connection);
        }
    }

    @Override
    public boolean addUser(AccountProto.User user, String password) {
        String query = "INSERT INTO " + USER_TABLE + " (user_id, user_type, full_name, qualification, email, address, " +
                "password, phone, is_deleted, created_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement statement = null;
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            logger.debug(QUERY, query);
            statement = dataSource.prepareStatement(query, connection);
            statement.setString(1, user.getUserId());
            statement.setByte(2, (byte) user.getUserType().getNumber());
            statement.setString(3, user.getName());
            statement.setByte(4, (byte) user.getQualification().getNumber());
            statement.setString(5, user.getEmail());
            statement.setString(6, user.getAddress());
            statement.setString(7, SavvyUtils.passwordHash(password));
            statement.setString(8, user.getPhone());
            statement.setBoolean(9, false);
            statement.setLong(10, user.getCreatedAt());
            return statement.executeUpdate() == 1;
        } catch (SQLException | JDBCException ex) {
            logger.error("Error while adding user", ex);
            return false;
        } finally {
            close(statement);
            close(connection);
        }
    }

    @Override
    public List<AccountProto.User> getStudents() throws SavvyException {
        String query = SELECT + USER_COLS + " FROM " + USER_TABLE + " WHERE user_type = ? AND is_deleted = ?";
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = dataSource.getConnection();
            logger.debug(QUERY, query);
            statement = dataSource.prepareStatement(query, connection);
            statement.setByte(1, (byte) AccountProto.UserType.STUDENT.getNumber());
            statement.setBoolean(2, false);
            ResultSet resultSet = statement.executeQuery();

            return getResults(resultSet, this::extractUser);
        } catch (SQLException | JDBCException ex) {
            throw new SavvyException("Error while getting students", ex);
        } finally {
            close(statement);
            close(connection);
        }
    }

    @Override
    public List<AccountProto.User> getCompanies() throws SavvyException {
        String query = SELECT + USER_COLS + " FROM " + USER_TABLE + " WHERE user_type = ? AND is_deleted = ?";
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = dataSource.getConnection();
            logger.debug(QUERY, query);
            statement = dataSource.prepareStatement(query, connection);
            statement.setInt(1, AccountProto.UserType.COMPANY.getNumber());
            statement.setBoolean(2, false);
            ResultSet resultSet = statement.executeQuery();

            return getResults(resultSet, this::extractUser);
        } catch (SQLException | JDBCException ex) {
            throw new SavvyException("Error while getting companies", ex);
        } finally {
            close(statement);
            close(connection);
        }
    }

    @Override
    public AccountProto.User getUserById(String userId) throws SavvyException {
        String query = SELECT + USER_COLS + " FROM " + USER_TABLE + " WHERE user_id = ? AND is_deleted = ?";
        return getUser(userId, query);
    }

    @Override
    public AccountProto.User getUserByEmail(String email) throws SavvyException {
        String query = SELECT + USER_COLS + " FROM " + USER_TABLE + " WHERE email = ? AND is_deleted = ?";
        return getUser(email, query);
    }

    private AccountProto.User getUser(String factor, String query) throws SavvyException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = dataSource.getConnection();
            logger.debug(QUERY, query);
            statement = dataSource.prepareStatement(query, connection);
            statement.setString(1, factor);
            statement.setBoolean(2, false);
            ResultSet resultSet = statement.executeQuery();
            return getResult(resultSet, this::extractUser);
        } catch (SQLException | JDBCException ex) {
            throw new SavvyException("Error while getting user", ex);
        } finally {
            close(statement);
            close(connection);
        }
    }

    private AccountProto.User extractUser(ResultSet resultSet) {
        try {
            AccountProto.User.Builder user = AccountProto.User.newBuilder();
            user.setUserId(resultSet.getString("user_id"));
            user.setUserType(AccountProto.UserType.forNumber(resultSet.getByte("user_type")));
            user.setName(resultSet.getString("full_name"));
            user.setQualification(AccountProto.Qualification.forNumber(resultSet.getByte("qualification")));
            user.setEmail(resultSet.getString("email"));
            user.setAddress(resultSet.getString("address"));
            user.setPhone(resultSet.getString("phone"));
            user.setCreatedAt(resultSet.getLong("created_at"));
            user.setUpdatedAt(resultSet.getLong("updated_at"));
            return user.build();
        } catch (SQLException ex) {
            logger.error("Error while getting user resultset", ex);
            return null;
        }
    }
}

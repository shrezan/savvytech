package com.savvytech.repo;

import com.savvytech.sql.AbstractConnectionFactory;
import com.savvytech.sql.DbInfo;
import com.savvytech.sql.JDBCException;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigException;
import com.typesafe.config.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/
public final class SavvytechDataFactory extends AbstractConnectionFactory {
    private static final Logger logger = LoggerFactory.getLogger(SavvytechDataFactory.class);
    private static final Config config = ConfigFactory.load();
    private static final SavvytechDataFactory INSTANCE = new SavvytechDataFactory();

    private SavvytechDataFactory() {
        try {
            this.start();
        } catch (JDBCException e) {
            logger.error("Error while initializing db connection.", e);
        }
    }

    public static SavvytechDataFactory getInstance() {
        return INSTANCE;
    }

    @Override
    public DbInfo config() {
        DbInfo dbInfo = new DbInfo();
        dbInfo.setValidationQuery(config.getString("savvytech.db.validation.query"));
        dbInfo.setPassword(config.getString("savvytech.db.password"));
        dbInfo.setUsername(config.getString("savvytech.db.user"));
        dbInfo.setDriver(config.getString("savvytech.db.driver"));
        dbInfo.setDatabaseName(config.getString("savvytech.db.name"));
        String url;
        try {
            url = config.getString("savvytech.db.url");
        } catch (ConfigException.Missing ex) {
            url = String.format("jdbc:mysql://%s:%d/%s?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC",
                    config.getString("savvytech.db.host"),
                    config.getInt("savvytech.db.port"),
                    dbInfo.getDatabaseName());
        }
        dbInfo.setUrl(url);
        logger.debug("savvytech DB URL : {}", dbInfo.getUrl());
        return dbInfo;
    }
}

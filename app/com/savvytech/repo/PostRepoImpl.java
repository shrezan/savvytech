package com.savvytech.repo;

import com.google.inject.Inject;
import com.savvytech.entities.AccountProto;
import com.savvytech.ex.SavvyException;
import com.savvytech.sql.JDBCException;
import com.savvytech.sql.SqlDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-05
 **/
public final class PostRepoImpl extends PostRepo {
    private static final Logger logger = LoggerFactory.getLogger(PostRepoImpl.class);

    private static final String POST_TABLE = "post";
    private static final String USER_TABLE = "user";
    private static final String POST_COLS = "post_id, company_id, title, description, post_type, created_at, updated_at";

    private static final String POST_COMPANY_COLS = "p.post_id, p.company_id, p.title, p.description, p.post_type, " +
            "p.created_at, p.updated_at, " +
            "u.full_name, u.email, u.address";


    private static final String APPLY_TABLE = "post_apply";
    private static final String APPLY_COLS = "apply_id, student_id, post_id, cv_link, created_at, updated_at";
    private static final String APPLY_JOIN_COLS = "a.apply_id, a.post_id, a.student_id, a.cv_link, a.created_at, a.updated_at, " +
            "u.user_id, u.full_name, u.qualification, u.address, u.user_type, u.email, u.phone, u.created_at";

    private static final String QUERY = "Query : {}";
    private static final String SELECT = "SELECT ";
    private static final String FROM = " FROM ";

    @Inject
    public PostRepoImpl(SqlDataSource dataSource) {
        super(dataSource);
    }

    @Override
    public boolean addPost(AccountProto.Post post) {
        String query = "INSERT INTO " + POST_TABLE + " (post_id, post_type, company_id, title, description," +
                " created_at, is_deleted) VALUES (?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement statement = null;
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            logger.debug(QUERY, query);
            statement = dataSource.prepareStatement(query, connection);
            statement.setString(1, post.getPostId());
            statement.setByte(2, (byte) post.getPostType().getNumber());
            statement.setString(3, post.getCompanyId());
            statement.setString(4, post.getTitle());
            statement.setString(5, post.getDescription());
            statement.setLong(6, post.getCreatedAt());
            statement.setBoolean(7, false);
            return statement.executeUpdate() == 1;
        } catch (SQLException | JDBCException ex) {
            logger.error("Error while adding post", ex);
            return false;
        } finally {
            close(statement);
            close(connection);
        }
    }

    @Override
    public AccountProto.PostUser getPostById(String postId) throws SavvyException {
        String query = SELECT + POST_COMPANY_COLS + FROM + POST_TABLE + " as p INNER JOIN " +
                USER_TABLE + " as u ON p.company_id = u.user_id" +
                " WHERE post_id = ? AND p.is_deleted = ?";
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            logger.debug(QUERY, query);
            statement = dataSource.prepareStatement(query, connection);
            statement.setString(1, postId);
            statement.setBoolean(2, false);
            resultSet = statement.executeQuery();
            return getResult(resultSet, this::extractPostInfo);
        } catch (SavvyException | SQLException ex) {

            throw new SavvyException("Error while getting post", ex);
        } finally {
            close(statement);
            close(connection);
            close(resultSet);
        }
    }

    @Override
    public List<AccountProto.Post> getEvents() throws SavvyException {
        String query = SELECT + POST_COLS + FROM + POST_TABLE + " WHERE post_type = ?";
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            logger.debug(QUERY, query);
            statement = dataSource.prepareStatement(query, connection);
            statement.setByte(1, (byte) 1);
            resultSet = statement.executeQuery();
            return getResults(resultSet, this::extractPost);
        } catch (SQLException | JDBCException ex) {
            throw new SavvyException("Error while getting events", ex);
        } finally {
            close(statement);
            close(connection);
            close(resultSet);
        }
    }

    @Override
    public List<AccountProto.Post> getJobs(long to, int limit) throws SavvyException {
        String query = SELECT + POST_COLS + FROM + POST_TABLE +
                " WHERE created_at < ? AND is_deleted = ? ORDER BY created_at DESC LIMIT ?";
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            logger.debug(QUERY, query);
            statement = dataSource.prepareStatement(query, connection);
            statement.setLong(1, to);
            statement.setBoolean(2, false);
            statement.setInt(3, limit);
            resultSet = statement.executeQuery();
            return getResults(resultSet, this::extractPost);
        } catch (SQLException | JDBCException ex) {
            throw new SavvyException("Error while getting jobs", ex);
        } finally {
            close(statement);
            close(connection);
            close(resultSet);
        }
    }

    private AccountProto.Post extractPost(ResultSet resultSet) {
        try {
            AccountProto.Post.Builder post = AccountProto.Post.newBuilder();
            post.setPostId(resultSet.getString("post_id"));
            post.setPostType(AccountProto.PostType.forNumber(resultSet.getByte("post_type")));
            post.setCompanyId(resultSet.getString("company_id"));
            post.setTitle(resultSet.getString("title"));
            post.setDescription(resultSet.getString("description"));
            post.setCreatedAt(resultSet.getLong("created_at"));
            return post.build();
        } catch (SQLException ex) {
            logger.error("Error while getting post resultset", ex);
            return null;
        }
    }

    private AccountProto.PostUser extractPostInfo(ResultSet resultSet) {
        try {
            AccountProto.Post post = extractPost(resultSet);
            AccountProto.User user = extractUser(resultSet);
            return AccountProto.PostUser.newBuilder()
                    .setPost(post)
                    .setUser(user)
                    .build();
        } catch (Exception ex) {
            logger.error("Error while getting post postInfo resultset", ex);
            return null;
        }
    }

    private AccountProto.User extractUser(ResultSet resultSet) {
        try {
            AccountProto.User.Builder user = AccountProto.User.newBuilder();
            user.setName(resultSet.getString("full_name"));
            user.setEmail(resultSet.getString("email"));
            user.setAddress(resultSet.getString("address"));
            return user.build();
        } catch (SQLException ex) {
            logger.error("Error while getting post user resultset", ex);
            return null;
        }
    }

    @Override
    public boolean postApply(AccountProto.Apply studentApply) {
        String query = "INSERT INTO " + APPLY_TABLE + " (apply_id, student_id, post_id, cv_link, created_at)" +
                " VALUES (?, ?, ?, ?, ?)";
        PreparedStatement statement = null;
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            logger.debug(QUERY, query);
            statement = dataSource.prepareStatement(query, connection);
            statement.setString(1, studentApply.getApplyId());
            statement.setString(2, studentApply.getStudentId());
            statement.setString(3, studentApply.getPostId());
            statement.setString(4, studentApply.getCvLink());
            statement.setLong(5, studentApply.getCreatedAt());
            return statement.executeUpdate() == 1;
        } catch (SQLException | JDBCException ex) {
            logger.error("Error while applying post", ex);
            return false;
        } finally {
            close(statement);
            close(connection);
        }
    }

    @Override
    public List<AccountProto.Apply> getPostApplies(String postId, long to, int limit) throws SavvyException {
        String query = SELECT + APPLY_JOIN_COLS + FROM + APPLY_TABLE + " AS a" +
                " INNER JOIN " + USER_TABLE + " AS u ON a.student_id = u.user_id" +
                " WHERE a.post_id = ? AND a.created_at < ? ORDER BY a.created_at DESC LIMIT ?";
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            logger.debug(QUERY, query);
            statement = dataSource.prepareStatement(query, connection);
            statement.setString(1, postId);
            statement.setLong(2, to);
            statement.setInt(3, limit);
            resultSet = statement.executeQuery();
            return getResults(resultSet, this::extractApply);
        } catch (SQLException | JDBCException ex) {
            throw new SavvyException("Error while getting post applies", ex);
        } finally {
            close(statement);
            close(connection);
            close(resultSet);
        }
    }

    @Override
    public List<AccountProto.Post> getCompanyPosts(String companyId, long to, int limit) throws SavvyException {
        String query = SELECT + POST_COLS + FROM + POST_TABLE +
                " WHERE company_id = ? AND created_at < ? AND is_deleted = ? ORDER BY created_at DESC LIMIT ?";
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            logger.debug(QUERY, query);
            statement = dataSource.prepareStatement(query, connection);
            statement.setString(1, companyId);
            statement.setLong(2, to);
            statement.setBoolean(3, false);
            statement.setInt(4, limit);
            resultSet = statement.executeQuery();
            return getResults(resultSet, this::extractPost);
        } catch (SQLException | JDBCException ex) {
            throw new SavvyException("Error while getting company posts", ex);
        } finally {
            close(statement);
            close(connection);
            close(resultSet);
        }
    }

    @Override
    public List<AccountProto.Post> getPostsByPostType(AccountProto.PostType postType,
                                                      long to, int limit) throws SavvyException {
        String query = SELECT + POST_COLS + FROM + POST_TABLE +
                " WHERE post_type = ? AND created_at < ? AND is_deleted = ? ORDER BY created_at DESC LIMIT ?";
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            logger.debug(QUERY, query);
            statement = dataSource.prepareStatement(query, connection);
            statement.setByte(1, (byte) postType.getNumber());
            statement.setLong(2, to);
            statement.setBoolean(3, false);
            statement.setInt(4, limit);
            resultSet = statement.executeQuery();
            return getResults(resultSet, this::extractPost);
        } catch (SQLException | JDBCException ex) {
            throw new SavvyException("Error while getting company posts", ex);
        } finally {
            close(statement);
            close(connection);
            close(resultSet);
        }
    }

    @Override
    public boolean deletePost(String postId) {
        String query = "UPDATE " + POST_TABLE + " SET is_deleted = ? WHERE post_id = ?";
        PreparedStatement statement = null;
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            logger.debug(QUERY, query);
            statement = dataSource.prepareStatement(query, connection);
            statement.setBoolean(1, true);
            statement.setString(2, postId);
            return statement.executeUpdate() == 1;
        } catch (SQLException | JDBCException ex) {
            logger.error("Error while deleting post", ex);
            return false;
        } finally {
            close(statement);
            close(connection);
        }
    }

    @Override
    public boolean hasUserApplied(String userId, String postId) throws SavvyException {
        String query = SELECT + APPLY_COLS + FROM + APPLY_TABLE + " WHERE student_id = ? AND post_id = ?";
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            logger.debug(QUERY, query);
            statement = dataSource.prepareStatement(query, connection);
            statement.setString(1, userId);
            statement.setString(2, postId);
            resultSet = statement.executeQuery();
            return resultSet == null;
        } catch (SQLException | JDBCException ex) {
            throw new SavvyException("Error while checking user application", ex);
        } finally {
            close(statement);
            close(connection);
            close(resultSet);
        }
    }

    private AccountProto.Apply extractApply(ResultSet resultSet) {
        try {
            AccountProto.Apply.Builder apply = AccountProto.Apply.newBuilder();
            apply.setApplyId(resultSet.getString("apply_id"));
            apply.setPostId(resultSet.getString("post_id"));
            apply.setStudentId(resultSet.getString("student_id"));
            apply.setCvLink(resultSet.getString("cv_link"));
            apply.setCreatedAt(resultSet.getLong("a.created_at"));

            AccountProto.User.Builder user = AccountProto.User.newBuilder();
            user.setUserId(resultSet.getString("student_id"));
            user.setUserType(AccountProto.UserType.forNumber(resultSet.getByte("user_type")));
            user.setName(resultSet.getString("full_name"));
            user.setQualification(AccountProto.Qualification.forNumber(resultSet.getByte("qualification")));
            user.setEmail(resultSet.getString("email"));
            user.setAddress(resultSet.getString("address"));
            user.setPhone(resultSet.getString("phone"));
            user.setCreatedAt(resultSet.getLong("u.created_at"));
            apply.setUser(user);
            return apply.build();
        } catch (SQLException ex) {
            logger.error("Error while getting apply resultset", ex);
            return null;
        }
    }
}

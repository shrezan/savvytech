package com.savvytech.repo.model;

import com.savvytech.entities.AccountProto;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/
public final class LoginResult {
    public final String password;
    public final AccountProto.User user;

    public LoginResult(String password, AccountProto.User user) {
        this.password = password;
        this.user = user;
    }
}

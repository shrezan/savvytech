package com.savvytech.repo;

import com.savvytech.ex.SavvyException;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import io.minio.MinioClient;
import io.minio.errors.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmlpull.v1.XmlPullParserException;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-06-22
 **/
public final class FileRepoImpl extends FileRepo {
    private final Logger logger = LoggerFactory.getLogger(FileRepoImpl.class);
    private final MinioClient minioClient;

    @Inject
    public FileRepoImpl() throws SavvyException {
        try {
            Config config = ConfigFactory.load();
            this.minioClient = new MinioClient(
                    config.getString("minio.endpoint"),
                    config.getString("minio.access.key"),
                    config.getString("minio.secret.key"));
        } catch (InvalidEndpointException | InvalidPortException e) {
            throw new SavvyException(e.getMessage(), e);
        }
    }
    @Override
    public String generatePublicUrl(String bucket, String fileName){
        try {
            return this.minioClient.presignedGetObject(bucket, fileName);
        } catch (InvalidBucketNameException | InvalidExpiresRangeException
                | InternalException | ErrorResponseException
                | XmlPullParserException | NoResponseException
                | InvalidKeyException | IOException
                | InsufficientDataException | NoSuchAlgorithmException e) {
            logger.error("Error while generating url", e);
        }
        return null;
    }
    @Override
    public boolean uploadFile(String bucket, String file, String mime, InputStream inputStream){
        try {
            this.minioClient.putObject(bucket, file, inputStream, mime);
            return true;
        } catch (InvalidBucketNameException | NoSuchAlgorithmException
                | InsufficientDataException | IOException
                | InvalidKeyException | NoResponseException
                | XmlPullParserException | ErrorResponseException | InternalException
                | InvalidArgumentException e) {
            logger.error("Error while uploading file to minio.", e);
        }
        return false;
    }
}

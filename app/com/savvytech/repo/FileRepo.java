package com.savvytech.repo;

import java.io.InputStream;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-06-22
 **/
public abstract class FileRepo {
    protected FileRepo() {
    }

    public abstract String generatePublicUrl(String bucket, String fileName);

    public abstract boolean uploadFile(String bucket, String file, String mime, InputStream inputStream);
}

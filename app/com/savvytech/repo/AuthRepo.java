package com.savvytech.repo;

import com.google.protobuf.InvalidProtocolBufferException;
import com.savvytech.cache.Cache;
import com.savvytech.entities.AccountProto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/
public final class AuthRepo {
    private static final Logger logger = LoggerFactory.getLogger(AuthRepo.class);
    //In seconds
    private final Cache<byte[]> cache;

    @Inject
    public AuthRepo(final Cache<byte[]> cache) {
        this.cache = cache;
    }

    public boolean remove(final AccountProto.Session session) {
        AccountProto.JwtLit holder = get(session.getUserId());
        if (null == holder || holder.getJwtsList().size() == 0) return true;
        List<AccountProto.Jwt> sessions = holder.getJwtsList().stream()
                .filter(sess -> !session.getSessionId().equalsIgnoreCase(sess.getSession().getSessionId()))
                .collect(Collectors.toList());
        return this.set(session.getUserId(), AccountProto.JwtLit
                .newBuilder()
                .addAllJwts(sessions).build());
    }

    public List<AccountProto.Jwt> getSessions(final String userId) {
        AccountProto.JwtLit holder = get(userId);
        if (null == holder || holder.getJwtsList().size() == 0) return Collections.emptyList();
        return holder.getJwtsList();
    }

    public AccountProto.Jwt check(final String userId, final String sessionId) {
        AccountProto.JwtLit holder = get(userId);
        if (null == holder || holder.getJwtsList().size() == 0) return null;
        for (AccountProto.Jwt session : holder.getJwtsList()) {
            if (session.getSession().getSessionId().equalsIgnoreCase(sessionId)) {
                return session;
            }
        }
        return null;
    }

    private AccountProto.JwtLit get(final String userId) {
        try {
            byte[] content = cache.get(userId);
            AccountProto.JwtLit holder;
            if (null == content || content.length == 0) {
                List<AccountProto.Jwt> sessions = new LinkedList<>();
                AccountProto.JwtLit.Builder holderBuilder = AccountProto.JwtLit.newBuilder();
                holderBuilder.addAllJwts(sessions);
                holder = holderBuilder.build();
            } else {
                holder = AccountProto.JwtLit.parseFrom(content);
            }
            return holder;
        } catch (InvalidProtocolBufferException e) {
            logger.error("Exception while de serializing session", e);
        }
        return null;
    }

    private boolean set(String userId, AccountProto.JwtLit holder) {
        return cache.set(userId, holder.toByteArray());
    }

    public boolean update(final AccountProto.Jwt session) {
        AccountProto.JwtLit holder = get(session.getSession().getUserId());
        if (null == holder || holder.getJwtsList().size() == 0) return true;
        List<AccountProto.Jwt> sessions = holder.getJwtsList().stream()
                .filter(sess -> !session.getSession().getSessionId()
                        .equalsIgnoreCase(sess.getSession().getSessionId()))
                .collect(Collectors.toList());
        sessions.add(session);
        return this.set(session.getSession().getUserId(), AccountProto.JwtLit
                .newBuilder()
                .addAllJwts(sessions)
                .build());
    }

    public boolean save(final AccountProto.Jwt session) {
        AccountProto.JwtLit holder = get(session.getSession().getUserId());
        if (null == holder) return false;
        List<AccountProto.Jwt> sessions = new LinkedList<>();
        sessions.addAll(holder.getJwtsList());
        sessions.add(session);
        AccountProto.JwtLit sessionHolder = AccountProto.JwtLit
                .newBuilder()
                .addAllJwts(sessions).build();
        return this.set(session.getSession().getUserId(), sessionHolder);
    }
}

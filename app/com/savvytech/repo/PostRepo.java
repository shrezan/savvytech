package com.savvytech.repo;

import com.savvytech.entities.AccountProto;
import com.savvytech.ex.SavvyException;
import com.savvytech.sql.SqlDataSource;

import java.util.List;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-05
 **/
public abstract class PostRepo extends SqlRepo {
    protected PostRepo(SqlDataSource dataSource) {
        super(dataSource);
    }

    public abstract boolean addPost(AccountProto.Post user);

    public abstract AccountProto.PostUser getPostById(String postId) throws SavvyException;

    public abstract List<AccountProto.Post> getEvents() throws SavvyException;

    public abstract List<AccountProto.Post> getJobs(long to, int limit) throws SavvyException;

    public abstract boolean postApply(AccountProto.Apply studentApply);

    public abstract List<AccountProto.Apply> getPostApplies(String postId, long to, int limit) throws SavvyException;

    public abstract List<AccountProto.Post> getCompanyPosts(String companyId, long to, int limit) throws SavvyException;

    public abstract List<AccountProto.Post> getPostsByPostType(AccountProto.PostType postType,
                                                               long to, int limit) throws SavvyException;

    public abstract boolean deletePost(String postId);

    public abstract boolean hasUserApplied(String userId, String postId) throws SavvyException;

}

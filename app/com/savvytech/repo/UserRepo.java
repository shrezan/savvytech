package com.savvytech.repo;

import com.savvytech.entities.AccountProto;
import com.savvytech.ex.SavvyException;
import com.savvytech.repo.model.LoginResult;
import com.savvytech.sql.SqlDataSource;

import java.util.List;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/
public abstract class UserRepo extends SqlRepo {
    protected UserRepo(SqlDataSource dataSource) {
        super(dataSource);
    }

    public abstract LoginResult login(String email) throws SavvyException;

    public abstract boolean addUser(AccountProto.User user, String password);

    public abstract List<AccountProto.User> getStudents() throws SavvyException;

    public abstract List<AccountProto.User> getCompanies() throws SavvyException;

    public abstract AccountProto.User getUserById(String userId) throws SavvyException;

    public abstract AccountProto.User getUserByEmail(String email) throws SavvyException;

}

package com.savvytech.ex;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/
public class SavvyException extends Exception {
    public SavvyException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public SavvyException(String msg) {
        super(msg);
    }

    public SavvyException(Throwable throwable) {
        super(throwable);
    }
}

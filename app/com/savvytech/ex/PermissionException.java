package com.savvytech.ex;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/
public class PermissionException extends AuthException {

    public PermissionException() {
        super("Permission denied.");
    }
}

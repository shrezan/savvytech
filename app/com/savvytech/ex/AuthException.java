package com.savvytech.ex;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/
public class AuthException extends SavvyException {
    public AuthException() {
        super("Authorization failed.");
    }

    public AuthException(String msg) {
        super(msg);
    }
}

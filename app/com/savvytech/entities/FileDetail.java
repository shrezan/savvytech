package com.savvytech.entities;

import java.io.InputStream;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-06-22
 **/
public final class FileDetail {
    private String originalName;
    private long fileSize;
    private InputStream stream;
    private byte[] bytes;
    private String mime;
    private boolean isBytes = false;
    public FileDetail() {
    }
    public String getOriginalName() {
        return this.originalName;
    }
    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }
    public long getFileSize() {
        return this.fileSize;
    }
    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }
    public InputStream getStream() {
        return this.stream;
    }
    public void setStream(InputStream stream) {
        this.stream = stream;
    }
    public String getMime() {
        return this.mime;
    }
    public void setMime(String mime) {
        this.mime = mime;
    }
    public byte[] getBytes() {
        return this.bytes;
    }
    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }
    public boolean isBytes() {
        return this.isBytes;
    }
    public void setBytes(boolean bytes) {
        this.isBytes = bytes;
    }
}

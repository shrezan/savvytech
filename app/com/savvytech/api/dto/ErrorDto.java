package com.savvytech.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public final class ErrorDto {
    private final String msg;
    private final boolean error;
    private static final String errorCode = "INVALID";

    public ErrorDto() {
        this("Json parse error.");
    }

    public ErrorDto(String msg) {
        this.msg = msg;
        this.error = true;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getMsg() {
        return msg;
    }

    public boolean isError() {
        return error;
    }
}

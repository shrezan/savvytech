package com.savvytech.api;

import play.mvc.Http;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/
public final class MediaTypes {
    public static final String JSON = Http.MimeTypes.JSON;
    public static final String PROTOBUFF = "application/protobuf";
}

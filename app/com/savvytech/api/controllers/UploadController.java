package com.savvytech.api.controllers;

import com.savvytech.api.servicecall.UploadServiceCall;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.concurrent.CompletionStage;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-06-22
 **/
public final class UploadController extends ProtoController {
    private final UploadServiceCall uploadServiceCall;

    @Inject
    public UploadController(UploadServiceCall uploadServiceCall) {
        this.uploadServiceCall = uploadServiceCall;
    }

    public CompletionStage<Result> uploadFile() {
        return parse(authorization -> uploadServiceCall.uploadFile(authorization, getFileObject("file")));
    }
}
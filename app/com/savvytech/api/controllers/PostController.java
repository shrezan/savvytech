package com.savvytech.api.controllers;

import com.savvytech.api.servicecall.PostServiceCall;
import com.savvytech.entities.AccountProto;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.concurrent.CompletionStage;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-05
 **/
public final class PostController extends ProtoController {
    private final com.savvytech.api.servicecall.PostServiceCall postServiceCall;

    @Inject
    public PostController(final PostServiceCall postServiceCall) {
        this.postServiceCall = postServiceCall;
    }

    public CompletionStage<Result> addPost() {
        return parse(AccountProto.Post.class, (post, authorization) ->
                postServiceCall.addPost((AccountProto.Post) post));
    }

    public CompletionStage<Result> getEvents() {
        return parse(postServiceCall::getEvents);
    }

    public CompletionStage<Result> getJobs(long to, int limit) {
        return parse(authorization -> postServiceCall.getJobs(authorization, to, limit));
    }

    public CompletionStage<Result> getPostById(String postId) {
        return parse(authorization -> postServiceCall.getPostById(authorization, postId));
    }

    public CompletionStage<Result> postApply() {
        return parse(AccountProto.Apply.class, (apply, authorization) ->
                postServiceCall.postApply(authorization, (AccountProto.Apply) apply));
    }

    public CompletionStage<Result> getPostApplies(String postId, long to, int limit) {
        return parse(authorization -> postServiceCall.getPostApplies(authorization, postId, to, limit));
    }

    public CompletionStage<Result> getCompanyPosts(String companyId, long to, int limit) {
        return parse(authorization -> postServiceCall.getCompanyPosts(authorization, companyId, to, limit));
    }

    public CompletionStage<Result> getPostsByPostType(String postType, long to, int limit) {
        return parse(authorization -> postServiceCall.getPostsByPostType(authorization, postType, to, limit));
    }

    public CompletionStage<Result> deletePost(String postId) {
        return parse(authorization -> postServiceCall.deletePost(authorization, postId));
    }
}

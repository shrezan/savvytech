package com.savvytech.api.controllers;

import com.savvytech.api.servicecall.AccountServiceCall;
import com.savvytech.entities.AccountProto;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.concurrent.CompletionStage;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/
public final class AccountController extends ProtoController {
    private final AccountServiceCall accountServiceCall;

    @Inject
    public AccountController(final AccountServiceCall accountServiceCall) {
        this.accountServiceCall = accountServiceCall;
    }

    public CompletionStage<Result> login() {
        return parse(AccountProto.LoginRequest.class, loginRequest -> accountServiceCall.login
                ((AccountProto.LoginRequest) loginRequest));
    }

    public CompletionStage<Result> logout() {
        return parse(accountServiceCall::logout);
    }
}

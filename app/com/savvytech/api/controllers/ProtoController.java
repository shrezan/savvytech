package com.savvytech.api.controllers;

import akka.util.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Message;
import com.google.protobuf.util.JsonFormat;
import com.savvytech.api.dto.ErrorDto;
import com.savvytech.api.dto.TestDto;
import com.savvytech.api.executors.ServiceCallExecutor;
import com.savvytech.entities.AccountProto;
import com.savvytech.entities.FileDetail;
import com.savvytech.ex.SavvyException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.libs.Json;
import play.libs.concurrent.HttpExecution;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

import javax.inject.Inject;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.reflect.Method;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.Executor;
import java.util.function.BiFunction;
import java.util.function.Function;

import static com.savvytech.api.MediaTypes.JSON;
import static com.savvytech.api.MediaTypes.PROTOBUFF;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/
abstract class ProtoController extends Controller {
    private static final Logger logger = LoggerFactory.getLogger(ProtoController.class);
    @Inject
    private ServiceCallExecutor serviceCallExecutor;

    Executor getExecutor() {
        return HttpExecution.fromThread((Executor) serviceCallExecutor);
    }

    <T extends Message> CompletionStage<Result> parse(Function<AccountProto.Authorization,
            CompletionStage<Message>> callback) {
        try {
            Optional<String> authHeader = request().getHeaders().get(AUTHORIZATION);
            if (authHeader.isPresent()) {
                return result(callback.apply(AccountProto.Authorization
                        .newBuilder()
                        .setToken(authHeader.get())
                        .build()));
            } else {
                return CompletableFuture
                        .completedFuture(badRequest(Json.toJson(new ErrorDto("Authorization missing."))
                                .toString()));
            }
        } catch (Exception e) {
            logger.error("Error while parsing.", e);
        }
        return CompletableFuture
                .completedFuture(badRequest(Json.toJson(new ErrorDto("Format error"))
                        .toString()));
    }

    <T extends Message> CompletionStage<Result> parse(Class<T> clazz,
                                                      BiFunction<Message,
                                                              AccountProto.Authorization,
                                                              CompletionStage<Message>> callback) {
        try {
            Optional<String> authHeader = request().getHeaders().get(Http.HeaderNames.AUTHORIZATION);
            if (authHeader.isPresent()) {
                return result(callback.apply(parse(clazz),
                        AccountProto.Authorization.newBuilder()
                                .setToken(authHeader.get())
                                .build()));
            } else {
                return CompletableFuture
                        .completedFuture(badRequest(Json.toJson(new ErrorDto("Authorization missing."))
                                .toString()));
            }
        } catch (SavvyException e) {
            logger.error("Error while parsing.", e);
        }
        return CompletableFuture
                .completedFuture(badRequest(Json.toJson(new ErrorDto("Format error"))
                        .toString()));
    }

    <T extends Message> CompletionStage<Result> parse(Class<T> clazz,
                                                      Function<Message, CompletionStage<Message>> callback) {
        try {
            return result(callback.apply(parse(clazz)));
        } catch (SavvyException e) {
            logger.error("Error while parsing.", e);
        }
        return CompletableFuture
                .completedFuture(badRequest(Json.toJson(new ErrorDto("Format error"))
                        .toString()));
    }

    private <T extends Message> T parse(Class<T> clazz) throws SavvyException {
        try {
            Optional<String> contentTypeHeader = request().getHeaders().get(Http.HeaderNames.CONTENT_TYPE);
            if (contentTypeHeader.isPresent() && contentTypeHeader.get().equalsIgnoreCase(JSON)) {
                Method builderMethod = clazz.getMethod("newBuilder");
                Message.Builder builder = (Message.Builder) builderMethod.invoke(clazz);
                JsonFormat.parser().merge(request().body().asJson().toString(), builder);
                return (T) builder.build();

            }
            Method parseMethod = clazz.getMethod("parseFrom", byte[].class);
            ByteString byteString = request().body().asBytes();
            return (T) parseMethod.invoke(clazz, byteString.toArray());
        } catch (Exception ex) {
            throw new SavvyException("", ex);
        }
    }

    CompletionStage<Result> jsonResult(Function<Message, TestDto> apply,
                                       CompletionStage<Message> result) {
        return result.thenApplyAsync(r -> ok(Json.toJson(apply.apply(r)))
                .as(JSON), getExecutor());
    }

    CompletionStage<Result> result(CompletionStage<Message> result) {
        try {
            Function<Message, Result> function;
            Optional<String> acceptHeader = request().getHeaders().get(ACCEPT);
            if (acceptHeader.isPresent()) {
                if (JSON.equalsIgnoreCase(acceptHeader.get().toLowerCase())) {
                    function = r -> ok(protoToJson(r)).as(JSON);
                } else {
                    function = r -> ok(r.toByteArray()).as(PROTOBUFF);
                }
            } else {
                function = r -> ok(r.toByteArray()).as(PROTOBUFF);
            }
            return result.thenApplyAsync(function, getExecutor());
        } catch (Exception ex) {
            logger.error("While parsing response.", ex);
        }
        return CompletableFuture
                .completedFuture(internalServerError(Json
                        .toJson(new com.savvytech.api.dto.ErrorDto("Response parse error"))
                        .toString()));
    }

    FileDetail getFileObject(String key) {
        FileDetail fileDetail = null;
        try {
            Http.MultipartFormData<File> multipartFormData = request().body().asMultipartFormData();
            Http.MultipartFormData.FilePart<File> fileData = multipartFormData.getFile(key);
            if (fileData != null) {
                File file = fileData.getFile();
                fileDetail = new FileDetail();
                FileInputStream inputStream = new FileInputStream(file);
                fileDetail.setMime(fileData.getContentType());
                fileDetail.setStream(inputStream);
                fileDetail.setOriginalName(fileData.getFilename());
            }
            return fileDetail;

        } catch (FileNotFoundException ex) {
            logger.error("Error while getting file.", ex);
            return fileDetail;
        }
    }

    private String protoToJson(Message messageV3) {
        try {
            return JsonFormat.printer().print(messageV3);
        } catch (InvalidProtocolBufferException e) {
            logger.error("Error while converting to json.", e);
        }
        return Json.toJson(new ErrorDto()).toString();
    }
}

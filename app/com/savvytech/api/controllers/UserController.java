package com.savvytech.api.controllers;

import com.savvytech.api.servicecall.UserServiceCall;
import com.savvytech.entities.AccountProto;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.concurrent.CompletionStage;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/
public final class UserController extends com.savvytech.api.controllers.ProtoController {
    private final UserServiceCall userServiceCall;

    @Inject
    public UserController(final UserServiceCall userServiceCall) {
        this.userServiceCall = userServiceCall;
    }

    public CompletionStage<Result> addUser() {
        return parse(AccountProto.AddUser.class, addUser -> userServiceCall.addUser((AccountProto.AddUser) addUser));
    }

    public CompletionStage<Result> getStudents() {
        return parse(userServiceCall::getStudents);
    }

    public CompletionStage<Result> getCompanies() {
        return parse(userServiceCall::getCompanies);
    }

    public CompletionStage<Result> getUserById(String userId) {
        return parse(authorization -> userServiceCall.getUserById(authorization, userId));
    }

    public CompletionStage<Result> getUserByEmail(String email) {
        return parse(authorization -> userServiceCall.getUserByEmail(authorization, email));
    }
}


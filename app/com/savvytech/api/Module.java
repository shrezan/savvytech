package com.savvytech.api;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import com.savvytech.cache.Cache;
import com.savvytech.cache.RedisCacheManager;
import com.savvytech.repo.*;
import com.savvytech.services.*;
import com.savvytech.sql.SqlDataSource;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/
public final class Module extends AbstractModule {
    @Override
    protected void configure() {
        RedisCacheManager cacheManager = new RedisCacheManager();
        cacheManager.start();
        bind(new TypeLiteral<Cache<byte[]>>() {
        }).toInstance(cacheManager.getCache());
        SqlDataSource sqlDataSource = new SqlDataSource(SavvytechDataFactory.getInstance());
        bind(SqlDataSource.class).toInstance(sqlDataSource);
        bind(UserRepo.class).to(UserRepoImpl.class);
        bind(PostRepo.class).to(PostRepoImpl.class);
        bind(FileRepo.class).to(FileRepoImpl.class);
        bind(AccountService.class).to(AccountServiceImpl.class);
        bind(UserService.class).to(UserServiceImpl.class);
        bind(PostService.class).to(PostServiceImpl.class);
        bind(FileUploadService.class).to(FileUploadServiceImpl.class);
    }
}

package com.savvytech.api.servicecall;

import com.google.protobuf.Message;
import com.savvytech.entities.AccountProto;
import com.savvytech.services.PostService;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-05
 **/
public class PostServiceCall implements ServiceCall {
    private final com.savvytech.services.PostService postService;

    @Inject
    public PostServiceCall(final PostService postService) {
        this.postService = postService;
    }

    public CompletionStage<Message> addPost(AccountProto.Post post) {
        return CompletableFuture.supplyAsync(() ->
                postService.addPost(post), getExecutor());
    }

    public CompletionStage<Message> getEvents(AccountProto.Authorization authorization) {
        return CompletableFuture.supplyAsync(() ->
                postService.getEvents(authorization), getExecutor());
    }

    public CompletionStage<Message> getJobs(AccountProto.Authorization authorization, long to, int limit) {
        return CompletableFuture.supplyAsync(() ->
                postService.getJobs(authorization, to, limit), getExecutor());
    }

    public CompletionStage<Message> getPostById(AccountProto.Authorization authorization, String postId) {
        return CompletableFuture.supplyAsync(() ->
                postService.getPostById(authorization, postId), getExecutor());
    }

    public CompletionStage<Message> postApply(AccountProto.Authorization authorization, AccountProto.Apply postApply) {
        return CompletableFuture.supplyAsync(() ->
                postService.postApply(authorization, postApply), getExecutor());
    }

    public CompletionStage<Message> getPostApplies(AccountProto.Authorization authorization,
                                                   String postId, long to, int limit) {
        return CompletableFuture.supplyAsync(() ->
                postService.getPostApplies(authorization, postId, to, limit), getExecutor());
    }

    public CompletionStage<Message> getCompanyPosts(AccountProto.Authorization authorization,
                                                    String companyId, long to, int limit) {
        return CompletableFuture.supplyAsync(() ->
                postService.getCompanyPosts(authorization, companyId, to, limit), getExecutor());
    }

    public CompletionStage<Message> getPostsByPostType(AccountProto.Authorization authorization,
                                                       String postType, long to, int limit) {
        return CompletableFuture.supplyAsync(() ->
                postService.getPostsByPostType(authorization, postType, to, limit), getExecutor());
    }

    public CompletionStage<Message> deletePost(AccountProto.Authorization authorization,
                                               String postId) {
        return CompletableFuture.supplyAsync(() ->
                postService.deletePost(authorization, postId), getExecutor());
    }
}

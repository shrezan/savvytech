package com.savvytech.api.servicecall;

import com.google.protobuf.Message;
import com.savvytech.entities.AccountProto;
import com.savvytech.services.AccountService;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/
public class AccountServiceCall implements ServiceCall {
    private final AccountService accountService;

    @Inject
    public AccountServiceCall(final AccountService accountService) {
        this.accountService = accountService;
    }

    public CompletionStage<Message> login(AccountProto.LoginRequest loginRequest) {
        return CompletableFuture.supplyAsync(() -> accountService.login(loginRequest),
                getExecutor());
    }

    public CompletionStage<Message> logout(AccountProto.Authorization authorization) {
        return CompletableFuture.supplyAsync(() -> accountService.logout(authorization),
                getExecutor());
    }
}

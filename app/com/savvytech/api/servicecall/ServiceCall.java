package com.savvytech.api.servicecall;

import com.savvytech.api.executors.ServiceCallExecutor;
import play.libs.concurrent.HttpExecution;

import javax.inject.Inject;
import java.util.concurrent.Executor;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/
public interface ServiceCall {
    @Inject
    ServiceCallExecutor serviceCallExecutor = null;

    default Executor getExecutor() {
        return HttpExecution.fromThread((Executor) serviceCallExecutor);
    }

    default ServiceCallExecutor getServiceCallExecutor() {
        return serviceCallExecutor;
    }
}

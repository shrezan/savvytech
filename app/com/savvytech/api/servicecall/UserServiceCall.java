package com.savvytech.api.servicecall;

import com.google.protobuf.Message;
import com.savvytech.entities.AccountProto;
import com.savvytech.services.UserService;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/
public class UserServiceCall implements ServiceCall {
    private final UserService userService;

    @Inject
    public UserServiceCall(final UserService userService) {
        this.userService = userService;
    }

    public CompletionStage<Message> addUser(AccountProto.AddUser addUser) {
        return CompletableFuture.supplyAsync(() -> userService.addUser(addUser.getUser(), addUser.getPassword()),
                getExecutor());
    }

    public CompletionStage<Message> getStudents(AccountProto.Authorization authorization) {
        return CompletableFuture.supplyAsync(() -> userService.getStudents(authorization),
                getExecutor());
    }

    public CompletionStage<Message> getCompanies(AccountProto.Authorization authorization) {
        return CompletableFuture.supplyAsync(() -> userService.getCompanies(authorization),
                getExecutor());
    }

    public CompletionStage<Message> getUserById(AccountProto.Authorization authorization, String userId) {
        return CompletableFuture.supplyAsync(() -> userService.getUserById(authorization, userId),
                getExecutor());
    }

    public CompletionStage<Message> getUserByEmail(AccountProto.Authorization authorization, String email) {
        return CompletableFuture.supplyAsync(() -> userService.getUserByEmail(authorization, email),
                getExecutor());
    }
}

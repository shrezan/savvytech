package com.savvytech.api.servicecall;

import com.google.protobuf.Message;
import com.savvytech.entities.AccountProto;
import com.savvytech.entities.FileDetail;
import com.savvytech.services.FileUploadService;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2020-06-22
 **/
public class UploadServiceCall implements ServiceCall {
    private final FileUploadService fileUploadService;

    @Inject
    public UploadServiceCall(FileUploadService fileUploadService) {
        this.fileUploadService = fileUploadService;
    }

    public CompletionStage<Message> uploadFile(AccountProto.Authorization authorization,
                                               FileDetail fileDetail) {
        return CompletableFuture.supplyAsync(() ->
                fileUploadService.uploadFile(authorization, fileDetail), getExecutor());
    }
}

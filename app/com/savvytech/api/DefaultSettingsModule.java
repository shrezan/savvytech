package com.savvytech.api;

import com.google.inject.AbstractModule;
import com.savvytech.scripts.DefaultUser;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/

public final class DefaultSettingsModule extends AbstractModule {
    @Override
    protected void configure() {
        DefaultUser.main(null);
    }
}

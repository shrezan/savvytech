package com.savvytech.api.executors;

import akka.actor.ActorSystem;
import play.libs.concurrent.CustomExecutionContext;

import javax.inject.Inject;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/
public final class ServiceCallExecutor extends CustomExecutionContext {
    @Inject
    public ServiceCallExecutor(ActorSystem actorSystem) {
        super(actorSystem, "service.call.dispatcher");
    }
}

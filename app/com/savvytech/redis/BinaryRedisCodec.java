package com.savvytech.redis;

import com.lambdaworks.redis.codec.RedisCodec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;

/**
 * Created by Shreejan Raj Joshi
 * Email joshishreejan@gmail.com
 * Created on 2019-11-04
 **/
public final class BinaryRedisCodec implements RedisCodec<String, byte[]> {
    private Charset charset = Charset.forName("UTF-8");
    private static final Logger logger = LoggerFactory.getLogger(BinaryRedisCodec.class);

    @Override
    public String decodeKey(ByteBuffer byteBuffer) {
        CharsetDecoder decoder = charset.newDecoder();
        try {
            return decoder.decode(byteBuffer).toString();
        } catch (CharacterCodingException e) {
            logger.error("Error while decoding", e);
        }
        return null;
    }

    @Override
    public byte[] decodeValue(ByteBuffer byteBuffer) {
        byte[] bytes = new byte[byteBuffer.remaining()];
        byteBuffer.get(bytes);
        return bytes;
    }

    @Override
    public ByteBuffer encodeKey(String s) {
        CharsetEncoder encoder = charset.newEncoder();
        try {
            return encoder.encode(CharBuffer.wrap(s));
        } catch (CharacterCodingException e) {
            logger.error("Error while encoding", e);
        }
        return null;
    }

    @Override
    public ByteBuffer encodeValue(byte[] bytes) {
        return ByteBuffer.wrap(bytes);
    }
}

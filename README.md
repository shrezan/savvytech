# Savvytech_backend
Savvytech back-end using Play framework.

## Dependencies

1. Mysql server (5.7) ([reference](https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-18-04))
2. Redis ([reference](https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-redis-on-ubuntu-18-04))
3. Minio ([reference](https://docs.min.io/docs/minio-client-complete-guide.html))

## Deployment

-> Run minio client 
```bash
sudo ./minio server /mnt/data
```

-> Create savvytech database

-> Run the api
```bash
./gradlew run
```
